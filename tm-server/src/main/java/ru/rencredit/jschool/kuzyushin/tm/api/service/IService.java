package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;

import java.util.List;

public interface IService <T extends AbstractEntity> {

    @NotNull
    List<T> findAll();

    void load(@Nullable List<T> ts);

    void load(@Nullable T[] ts);
}
