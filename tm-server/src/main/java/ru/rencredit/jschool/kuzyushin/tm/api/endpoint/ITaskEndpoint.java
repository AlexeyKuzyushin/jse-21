package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import javax.jws.WebMethod;
import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    List<Task> findAllTasks(@Nullable Session session);

    void clearTasks(@Nullable Session session) throws Exception;

    void createTask(@Nullable Session session, @Nullable String name, @Nullable String description);

    @Nullable
    Task findTaskById(@Nullable Session session, @Nullable String id);

    @Nullable
    Task findTaskByName(@Nullable Session session, @Nullable String name);

    @Nullable
    Task removeTaskById(@Nullable Session session, @Nullable String id);

    @Nullable
    Task removeTaskByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Task updateTaskById(@Nullable Session session, @Nullable String id,
                        @Nullable String name, @Nullable String description);
}
