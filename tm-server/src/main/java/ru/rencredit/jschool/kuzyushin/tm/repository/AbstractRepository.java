package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository <E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> records = new ArrayList<>();

    @NotNull
    public List<E> findAll() {
        return records;
    }

    @Override
    public void add(final @NotNull List<E> es) {
        for (final @NotNull E e: es) add(e);
    }

    @Override
    public void add(final @NotNull E[] es) {
        for (final @NotNull E e: es) add(e);
    }

    @NotNull
    @Override
    public E add(final @NotNull E e) {
        records.add(e);
        return e;
    }

    @Override
    public void load(final @NotNull List<E> es) {
        clear();
        add(es);
    }

    @Override
    public void load(final @NotNull E[] es) {
        clear();
        add(es);
    }

    @NotNull
    @Override
    public E remove(@NotNull E e) {
        records.remove(e);
        return e;
    }

    public void clear() {
        records.clear();
    }
}
