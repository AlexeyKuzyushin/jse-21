package ru.rencredit.jschool.kuzyushin.tm.service;

import ru.rencredit.jschool.kuzyushin.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    private final String NAME = "/application.properties";

    private final Properties properties = new Properties();

    {
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() throws Exception{
        final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        properties.load(inputStream);
    }

    @Override
    public String getServerHost() {
        final String propertyHost = properties.getProperty("server.host");
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Override
    public Integer getServerPort() {
        final String propertyPort = properties.getProperty("server.port");
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }
}
