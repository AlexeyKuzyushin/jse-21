package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void add(@NotNull String userId, @NotNull Session session);

    void remove(@NotNull String userId, @NotNull Session session);

    void clear(@NotNull String userId);

    @NotNull
    List<Session> findAllSessions (@NotNull String useId);

    @NotNull
    Session findSessionByUserId(@NotNull String userId) throws Exception;

    @NotNull
    Session findSessionById(@NotNull String id) throws Exception;

    @NotNull
    Session removeSessionByUserId(@NotNull String userId) throws Exception;

    @NotNull
    boolean contains(@NotNull String id) throws Exception;
}
