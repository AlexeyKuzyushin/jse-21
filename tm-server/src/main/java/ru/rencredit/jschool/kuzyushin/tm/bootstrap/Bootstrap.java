package ru.rencredit.jschool.kuzyushin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.ISessionEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ISessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.*;
import ru.rencredit.jschool.kuzyushin.tm.repository.ProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.SessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.TaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.UserRepository;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.service.*;

import javax.xml.ws.Endpoint;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(
            taskService, projectService, userService, authService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    private void initUsers() {
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("test", "test", Role.USER);
    }

    public void run(){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        initUsers();
        initEndpoint();
    }

    private void initEndpoint() {
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(sessionEndpoint);
        registry(dataEndpoint);
        registry(adminUserEndpoint);
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final Integer port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    public IDataService getDataService() { return dataService; }
}
