package ru.rencredit.jschool.kuzyushin.tm.api.service;

public interface IPropertyService {

    String getServerHost();

    Integer getServerPort();

    String getSessionSalt();

    Integer getSessionCycle();
}
