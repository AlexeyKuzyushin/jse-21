package ru.rencredit.jschool.kuzyushin.tm.api.service;

public interface IDataService {

    void saveDataBinary() throws Exception;

    void saveDataJson() throws Exception;

    void saveDataXml() throws Exception;

    void saveDataBase64() throws Exception;

    void loadDataBinary() throws Exception;

    void loadDataJson() throws Exception;

    void loadDataXml() throws Exception;

    void loadDataBase64() throws Exception;

    void clearDataBinary() throws Exception;

    void clearDataJson() throws Exception;

    void clearDataXml() throws Exception;

    void clearDataBase64() throws Exception;
}
