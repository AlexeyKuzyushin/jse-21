package ru.rencredit.jschool.kuzyushin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.util.HashUtil;

import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(final @NotNull IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyUserException();
        return userRepository.findById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeUser(final @Nullable User user) {
        if (user == null) throw new EmptyUserException();
        return userRepository.removeUser(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(final @Nullable String login,
                       final @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(final @Nullable String login,
                       final @Nullable String password,
                       final @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(final @Nullable String login,
                       final @Nullable String password,
                       final @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }
    @NotNull
    @Override
    @SneakyThrows
    public User updateLogin(final @Nullable String id,
                            final @Nullable String login) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findById(id);
        user.setLogin(login);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updatePassword(final @Nullable String id,
                               final @Nullable String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        user.setPasswordHash(password);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateEmail(final @Nullable String id,
                            final @Nullable String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(id);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateFirstName(final @Nullable String id,
                                final @Nullable String firstName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setFirstName(firstName);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateLastName(final @Nullable String id,
                               final @Nullable String lastName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setLastName(lastName);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateMiddleName(final @Nullable String id,
                                 final @Nullable String middleName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(true);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(false);
        return user;
    }
}
