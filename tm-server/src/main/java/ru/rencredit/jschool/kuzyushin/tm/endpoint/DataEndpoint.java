package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IDataEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class DataEndpoint implements IDataEndpoint {

    private IServiceLocator serviceLocator;

    public DataEndpoint() {
    }

    public DataEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void saveDataBinary(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataBinary();
    }

    @Override
    @WebMethod
    public void saveDataJson(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataJson();
    }

    @Override
    @WebMethod
    public void loadDataBinary(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataBinary();
    }

    @Override
    @WebMethod
    public void loadDataJson(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataJson();
    }

    @Override
    @WebMethod
    public void clearDataBinary(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().clearDataBinary();
    }

    @Override
    @WebMethod
    public void clearDataJson(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().clearDataJson();
    }

    @Override
    @WebMethod
    public void saveDataXml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataXml();
    }

    @Override
    @WebMethod
    public void loadDataXml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataXml();
    }

    @Override
    @WebMethod
    public void clearDataXml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().clearDataXml();
    }

    @Override
    public void saveDataBase64(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataBase64();
    }

    @Override
    public void loadDataBase64(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataBase64();
    }

    @Override
    public void clearDataBase64(
            @WebParam(name = "session", partName = "session") final @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().clearDataBase64();
    }
}
