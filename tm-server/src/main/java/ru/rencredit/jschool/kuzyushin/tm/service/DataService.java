package ru.rencredit.jschool.kuzyushin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IDataService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataService implements IDataService {

    private final IServiceLocator serviceLocator;

    public DataService(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void saveDataBinary() throws Exception {
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
             @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
        ) {
            objectOutputStream.writeObject(domain);
        }
    }

    @Override
    public void saveDataJson() throws Exception {
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        @NotNull final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String jsonData = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(jsonData.getBytes());
        }
    }

    @Override
    public void saveDataXml() throws Exception {
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        @NotNull final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xmlData = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(xmlData.getBytes());
        }
    }

    @Override
    public void saveDataBase64() throws Exception {
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();

        final byte[] originalData = byteArrayOutputStream.toByteArray();
        @NotNull final String encodedData = new BASE64Encoder().encode(originalData);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(encodedData.getBytes());
        fileOutputStream.close();

        byteArrayOutputStream.close();
    }

    @Override
    public void loadDataBinary() throws Exception {
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
             @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            serviceLocator.getDomainService().load(domain);
        }
    }

    @Override
    public void loadDataJson() throws Exception {
        @NotNull final String jsonData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(jsonData, Domain.class);
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void loadDataXml() throws Exception {
        @NotNull final String xmlData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML)));
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(xmlData, Domain.class);

        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void loadDataBase64() throws Exception {
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        @NotNull final String originalData = new String(Files.readAllBytes(file.toPath()));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(originalData);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);

        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    public void clearDataBinary() throws Exception {
        clearData(DataConstant.FILE_BINARY);
    }

    @Override
    public void clearDataJson() throws Exception {
        clearData(DataConstant.FILE_JSON);
    }

    @Override
    public void clearDataXml() throws Exception {
        clearData(DataConstant.FILE_XML);
    }

    @Override
    public void clearDataBase64() throws Exception {
        clearData(DataConstant.FILE_BASE64);
    }

    private void clearData(@Nullable final String fileName) throws Exception {
        if (fileName == null || fileName.isEmpty()) return;
        @NotNull final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
    }
}
