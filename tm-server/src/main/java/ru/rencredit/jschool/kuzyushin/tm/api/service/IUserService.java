package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User removeUser(@Nullable User user);

    @Nullable
    User removeById(@Nullable String id);

    @Nullable
    User removeByLogin(@Nullable String login);

    @NotNull
    User updateLogin(@Nullable String id, @Nullable String login);

    @NotNull
    User updatePassword(@Nullable String id, @Nullable String password);

    @Nullable
    User updateEmail(@Nullable String id, @Nullable String email);

    @Nullable
    User updateFirstName(@Nullable String id, @Nullable String firstName);

    @Nullable
    User updateLastName(@Nullable String id, @Nullable String lastName);

    @Nullable
    User updateMiddleName(@Nullable String id, @Nullable String middleName);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);
}
