package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

public interface IDataEndpoint {

    void saveDataBinary(@Nullable Session session) throws Exception;

    void saveDataJson(@Nullable Session session) throws Exception;

    void saveDataXml(@Nullable Session session) throws Exception;

    void saveDataBase64(@Nullable Session session) throws Exception;

    void loadDataBinary(@Nullable Session session) throws Exception;

    void loadDataJson(@Nullable Session session) throws Exception;

    void loadDataXml(@Nullable Session session) throws Exception;

    void loadDataBase64(@Nullable Session session) throws Exception;

    void clearDataBinary(@Nullable Session session) throws Exception;

    void clearDataJson(@Nullable Session session) throws Exception;

    void clearDataXml(@Nullable Session session) throws Exception;

    void clearDataBase64(@Nullable Session session) throws Exception;
}
