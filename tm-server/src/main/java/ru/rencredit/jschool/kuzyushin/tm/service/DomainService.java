package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;

public class DomainService implements IDomainService {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IAuthService authService;

    public DomainService(final @NotNull ITaskService taskService,
                         final @NotNull IProjectService projectService,
                         final @NotNull IUserService userService,
                         final @NotNull IAuthService authService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
        this.authService = authService;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        authService.logout();
        domain.setTasks(taskService.findAll());
        domain.setProjects(projectService.findAll());
        domain.setUsers(userService.findAll());
    }
}
