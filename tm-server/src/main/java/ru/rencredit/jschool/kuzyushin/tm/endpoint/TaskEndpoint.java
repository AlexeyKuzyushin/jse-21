package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.ITaskEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    private TaskEndpoint() {
    }

    public TaskEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<Task> findAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findALl(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id) {
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name) {
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id) {
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name) {
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description) {
        return serviceLocator.getTaskService().updateOneById(session.getUserId(), id, name, description);
    }
}
