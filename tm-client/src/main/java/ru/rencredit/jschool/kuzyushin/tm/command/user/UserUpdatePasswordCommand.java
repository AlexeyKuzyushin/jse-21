package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Role;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PASSWORD]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("ENTER PASSWORD:");
            @Nullable final String password = TerminalUtil.nextLine();
            serviceLocator.getUserEndpoint().updateUserPassword(session, password);
            System.out.println("[OK]");
            serviceLocator.getSessionEndpoint().closeSession(session);
            System.out.println("[PLEASE SIGN IN YOUR PROFILE AGAIN]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}
