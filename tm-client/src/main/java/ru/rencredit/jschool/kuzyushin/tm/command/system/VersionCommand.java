package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }
}
