package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        if (serviceLocator != null) {
            @Nullable final String id = TerminalUtil.nextLine();
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getTaskEndpoint().removeTaskById(session, id);;
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
