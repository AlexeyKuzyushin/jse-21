package ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Role;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;

public class DataJsonFasterXmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-fasterxml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to json file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON SAVE]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getDataEndpoint().saveDataJson(session);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
