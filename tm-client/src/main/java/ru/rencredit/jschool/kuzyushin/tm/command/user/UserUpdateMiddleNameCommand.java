package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateMiddleNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-middle-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user middle name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("ENTER MIDDLE NAME:");
            @Nullable final String middleName = TerminalUtil.nextLine();
            serviceLocator.getUserEndpoint().updateUserMiddleName(session, middleName);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
