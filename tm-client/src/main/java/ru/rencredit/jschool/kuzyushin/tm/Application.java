package ru.rencredit.jschool.kuzyushin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.Bootstrap;

public class Application
{
    public static void main(@Nullable final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
}
