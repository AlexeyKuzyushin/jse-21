package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    @NotNull
    List<AbstractCommand> getCommandList();
}
