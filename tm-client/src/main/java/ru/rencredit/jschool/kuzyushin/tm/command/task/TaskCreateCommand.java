package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASKS]");
        System.out.println("[ENTER NAME]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            @Nullable final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            @Nullable final String description = TerminalUtil.nextLine();
            serviceLocator.getTaskEndpoint().createTask(session, name, description);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
