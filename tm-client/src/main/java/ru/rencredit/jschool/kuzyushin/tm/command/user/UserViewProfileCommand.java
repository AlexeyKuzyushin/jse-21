package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserViewProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-view-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            @Nullable final User user = serviceLocator.getUserEndpoint().viewUserProfile(session);
            if (user == null) return;
            System.out.println("ID: " + user.getId());
            System.out.println("LOGIN: " + user.getLogin());
            System.out.println("E-mail: " + user.getEmail());
            System.out.println("FIRST NAME: " + user.getFirstName());
            System.out.println("LAST NAME: " + user.getLastName());
            System.out.println("MIDDLE NAME: " + user.getMiddleName());
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
